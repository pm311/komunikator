package com.example.test6;


import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity{


    private ArrayAdapter2 chatArrayAdapter;
    private static final String INTENT_ACTION_GRANT_USB
            = BuildConfig.APPLICATION_ID + ".GRANT_USB";
    static SerialIO serial;
    private ListView listView;
    private EditText chatText;
    private Button buttonSend;
    Context context;
    ChatData chatData;
    public String mesg;
    List<Wiadomosc> allMessages = new ArrayList<>();
    Integer currentChatId =null;
    static Context contextStatic;

    private static class Fred2 implements Runnable{
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    if(noConnectedDevices()){
                        SerialIO.connected=false;
                    }
                    Thread.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private static Fred2 fred2= new Fred2();

    static boolean noConnectedDevices(){
        UsbManager manager = (UsbManager) contextStatic.getSystemService(Context.USB_SERVICE);
        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().
                findAllDrivers(manager);
        return  availableDrivers.isEmpty();
    }

    private class Fred3 implements Runnable{
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    if(SerialIO.connected){
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (getSupportActionBar()==null)
                                    getActionBar().setTitle(chatData.getChatName(currentChatId) +
                                            " (online)");
                                else
                                    getSupportActionBar().setTitle(chatData.
                                            getChatName(currentChatId) + " (online)");
                                buttonSend.setText("wyslij");
                            }
                        });}
                    else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (getSupportActionBar()==null)
                                    getActionBar().setTitle(chatData.getChatName(currentChatId) +
                                            " (offline)");
                                else
                                    getSupportActionBar().setTitle(chatData.
                                            getChatName(currentChatId) + " (offline)");
                                buttonSend.setText("polacz");
                            }
                        });}
                    Thread.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
    private Fred3 fred3= new Fred3();

    private class Fred5 implements Runnable{
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(100);
                    if(SerialIO.connected) {
                        int y=chatArrayAdapter.getCount();
                        int z=chatData.getChatMsgNum(currentChatId)-y;
                        if(z>0){
                            for (int i=0;i<z;i++)
                                if (!chatData.readChatMsg(currentChatId,y+i).right){
                                    Wiadomosc temp=chatData.readChatMsg(currentChatId,
                                            y+i);
                                    if (!temp.getText().equals("null5"))
                                        allMessages.add(temp);
                                }
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    chatArrayAdapter.notifyDataSetChanged();
                                    listView.setSelection(chatArrayAdapter.getCount());
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private Fred5 fred5= new Fred5();

    void genericDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Komunikat");
        builder.setMessage(message);
        builder.setCancelable(true);
        AlertDialog dialog=null;
        DialogInterface.OnClickListener a=null;
        builder.setPositiveButton("zamknij", a);
        dialog= builder.create();
        AlertDialog finalDialog = dialog;
        a = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finalDialog.dismiss();
            }
        };
        dialog.show();
    }
    AlertDialog dialog2;
    static Thread thread2 = new Thread(fred2);
    Thread thread3 = new Thread(fred3);
    Thread thread5 = new Thread(fred5);

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    public void onPause() {
        super.onPause();
        thread3.interrupt();
        thread5.interrupt();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        context =getApplicationContext();
        contextStatic=getApplicationContext();
        if (thread3.getState() == Thread.State.NEW)
            thread3.start();
        if (thread5.getState() == Thread.State.NEW){
            thread5.start();
        }
        buttonSend = (Button) findViewById(R.id.buttonSend);
        buttonSend.setText("connect");

        listView = (ListView) findViewById(R.id.listView1);
        Intent i = getIntent();
        currentChatId = Integer.parseInt(i.getStringExtra("chatId"));
        chatData = new ChatData(getApplicationContext());
        allMessages =chatData.readAllChatMsg(currentChatId);
        chatArrayAdapter = new ArrayAdapter2(this,
                R.layout.dymek,allMessages);
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);
        listView.setSelection(listView.getCount());

        chatText = (EditText) findViewById(R.id.chatText);

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (noConnectedDevices()) {
                    genericDialog("Nie wykryto zadnych podlaczonych urzadzen");
                }
                else {
                    if (!SerialIO.connected) {
                        UsbManager manager = (UsbManager) context.getSystemService(
                                Context.USB_SERVICE);
                        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().
                                findAllDrivers(manager);
                        UsbSerialDriver driver = availableDrivers.get(0);
                        UsbSerialPort port = driver.getPorts().get(0);
                        int flags = PendingIntent.FLAG_IMMUTABLE;
                        PendingIntent usbPermissionIntent = PendingIntent.getBroadcast(context,
                                0, new Intent(INTENT_ACTION_GRANT_USB), flags);
                        manager.requestPermission(driver.getDevice(), usbPermissionIntent);
                        serial = new SerialIO(ChatActivity.this, manager,
                                availableDrivers, driver, port);
                        serial.poczatek();
                        if (serial.connection == null)
                            genericDialog("nie polaczono, poniewaz nie przyznano uprawnienia");
                        else {
                            if (!serial.zworkaPodlaczona()) {
                                genericDialog("nie polaczono, poniewaz nie podlaczono zworki");
                                try {
                                    serial.port.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                SerialIO.connected = false;
                            }
                            else {
                                SerialIO.connected = true;
                            }
                        }

                        if (thread2.getState() == Thread.State.NEW) {
                            thread2.setDaemon(true);
                            thread2.start();
                        }

                    }
                    else {
                        if (!chatText.getText().toString().equals("")) {
                              mesg =chatText.getText().toString();
                            byte[] rozmiar1 = mesg.getBytes(StandardCharsets.UTF_8);
                            int rozmiar2 = rozmiar1.length;
                            int rozmiar3 =0;
                            if (rozmiar2>237) {
                                rozmiar3 = rozmiar2 - 237;
                                genericDialog("nie wyslano wiadomosci, bo byla za dluga o "
                                        + rozmiar3 + " bajty");
                            }
                            else{
                                chatData.writeChatMsg(currentChatId, true, mesg);
                                if (currentChatId!=0)
                                    chatData.writeChatMsg(0, true, mesg);

                                chatText.setText("");

                                try {
                                    serial.wyslij3(mesg, currentChatId);
                                    Wiadomosc mesg2 = new Wiadomosc(true,mesg);
                                    allMessages.add(mesg2);
                                    chatArrayAdapter.notifyDataSetChanged();
                                    mesg = "";
                                    listView.setSelection(listView.getCount());
                                    } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        });

        if (getSupportActionBar()==null)
            getActionBar().setTitle(chatData.getChatName(currentChatId) + " (offline)");
        else
            getSupportActionBar().setTitle(chatData.getChatName(currentChatId) + " (offline)");
}}