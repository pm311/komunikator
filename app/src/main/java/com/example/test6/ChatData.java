package com.example.test6;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ChatData {
    Context context;
    List<String> allChatNames;

    public ChatData(Context context){
        this.context = context;
        this.allChatNames = getAllChatNames();

    }
    String getChatName(Integer id){
        SharedPreferences nazwaCzatu = context.getApplicationContext().
                getSharedPreferences("nazwyWiadomosci", 0);
        return nazwaCzatu.getString(formatChatId(id),"null4");
    }
    void setChatName (Integer id,String nazwa){
        SharedPreferences nazwaCzatu = context.getApplicationContext().
                getSharedPreferences("nazwyWiadomosci", 0);
        SharedPreferences.Editor edytor = nazwaCzatu.edit();
        edytor.putString(formatChatId(id), nazwa);
        edytor.apply();
        allChatNames.add(nazwa);
    }

    void createChat(Integer chatId,String nazwaCzatu){
        setChatName(chatId,nazwaCzatu);
        setMsgNumZero(chatId);
    }

    Integer getChatIdByName(String nazwa){
        SharedPreferences nazwaWiadomosci = context.getApplicationContext().
                getSharedPreferences("nazwyWiadomosci",0);
        Map<String, ?> allEntries = nazwaWiadomosci.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            if (entry.getValue().toString().equals(nazwa)&&!entry.getValue().toString().isEmpty()){
                return Integer.parseInt(entry.getKey());
            }
        }
        return null;
    }

    void writeChatMsg(Integer chatId, boolean isSender, String msg){
        SharedPreferences wiadomosci = context.getApplicationContext().
                getSharedPreferences("wiadomosci", 0);
        SharedPreferences.Editor edytor = wiadomosci.edit();
        edytor.putString(chatAndMsgId(chatId, getChatMsgNum(chatId)),msg);
        edytor.apply();
        setIsSender(chatId, getChatMsgNum(chatId),isSender);
        increaseMsgNum(chatId);
    }
    Wiadomosc readChatMsg(Integer chatId, Integer msgId){
        SharedPreferences wiadomosci = context.getApplicationContext().
                getSharedPreferences("wiadomosci", 0);
        String a = wiadomosci.getString(chatAndMsgId(chatId,msgId),"null5");
        Boolean b = getIsSender(chatId,msgId);
        return new Wiadomosc(b,a);
    }
    List<Wiadomosc> readAllChatMsg(Integer chatId){
        List<Wiadomosc> a = new ArrayList<>();
        int b = getChatMsgNum(chatId);
        for (int i=0;i<b;i++)
            a.add(readChatMsg(chatId,i));
        return a;
    }
    Integer getChatMsgNum(Integer chatId){
        SharedPreferences liczbaWiadomosci = context.getApplicationContext().
                getSharedPreferences("liczbaWiadomosci", 0);
        return liczbaWiadomosci.getInt(formatChatId(chatId),0);
    }
    void setMsgNumZero (Integer chatId){
        SharedPreferences liczbaWiadomosci = context.getApplicationContext().
                getSharedPreferences("liczbaWiadomosci", 0);
        SharedPreferences.Editor edytor = liczbaWiadomosci.edit();
        edytor.putInt(formatChatId(chatId), 0);
        edytor.apply();
    }
    void increaseMsgNum (Integer chatId){
        SharedPreferences liczbaWiadomosci = context.getApplicationContext().
                getSharedPreferences("liczbaWiadomosci", 0);
        SharedPreferences.Editor edytor = liczbaWiadomosci.edit();
        edytor.putInt(formatChatId(chatId), getChatMsgNum(chatId)+1);
        edytor.apply();
    }
    Boolean getIsSender (Integer chatId,Integer msgId){
        SharedPreferences isSender = context.getApplicationContext().
                getSharedPreferences("isSender", 0);
        return isSender.getBoolean(chatAndMsgId(chatId,msgId),false);
    }
    void setIsSender (Integer chatId,Integer msgId, boolean isSenderVal){
        SharedPreferences isSender = context.getApplicationContext().
                getSharedPreferences("isSender", 0);
        SharedPreferences.Editor edytor = isSender.edit();
        edytor.putBoolean(chatAndMsgId(chatId,msgId),isSenderVal);
        edytor.apply();
    }
    List<Integer> getAllChatId(){
        SharedPreferences ids = context.getApplicationContext().
                getSharedPreferences("nazwyWiadomosci", 0);
        Map<String, ?> allEntries = ids.getAll();
        List<Integer> ids2 = new ArrayList<Integer>();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            ids2.add(Integer.parseInt(entry.getKey()));
        }
        return ids2;
    }
    boolean checkChatExists(Integer chatId){
        for (int i=0; i<getAllChatId().size();i++){
            if (getAllChatId().get(i).equals(chatId))
                return true;
        }
        return false;
    }
    List<String> getAllChatNames(){
        List<String> names = new ArrayList<>();
        for (int i = 0; i< getAllChatId().size(); i++) {
            String s = getChatName(getAllChatId().get(i));
           if (!s.equals("null4"))
                names.add(s);
        }
        return names;
    }

    String chatAndMsgId(Integer chatId, Integer msgId){
        return formatChatId(chatId)+msgId.toString();
    }
    String formatChatId(Integer chatId){
        StringBuilder a= new StringBuilder(chatId.toString());
        while (a.length()<3)
            a.insert(0, "0");
        return new String(a);
    }
}
