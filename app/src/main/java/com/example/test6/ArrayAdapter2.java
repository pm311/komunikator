package com.example.test6;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ArrayAdapter2 extends ArrayAdapter<Wiadomosc> {


    private List<Wiadomosc> listaWiadomosci = new ArrayList<>();

    public ArrayAdapter2(Context context, int textViewResourceId, List<Wiadomosc>list) {
        super(context, textViewResourceId,list);
        this.listaWiadomosci.addAll(list);
    }


    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater)
                    this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.dymek, viewGroup, false);
        }
        LinearLayout dymek = (LinearLayout) view.findViewById(R.id.dymek);
        Wiadomosc wiadomosc = getItem(position);

        TextView chatText = (TextView) view.findViewById(R.id.wiadomosc);
        chatText.setText(wiadomosc.message);
        if (!wiadomosc.right) {
            chatText.setBackgroundResource(R.drawable.dymek2);
        }
        else {
            chatText.setBackgroundResource(R.drawable.dymek1);
        }
        if (!wiadomosc.right) {
            dymek.setGravity(Gravity.LEFT);
        }
        else {
            dymek.setGravity(Gravity.RIGHT);
        }
        return view;
    }
}