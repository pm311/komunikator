package com.example.test6;

import android.content.Context;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class SerialIO {
    Context context;
    UsbManager manager;
    List<UsbSerialDriver> availableDrivers;
    UsbSerialDriver driver;
    UsbSerialPort port;
    static UsbSerialPort portStatic;
    static final int znakPoczatkuWiadomosci = 0xfe;
    static final int znakKoncaWiadomosci = 0xff;
    static BlockingQueue<Byte> received = new LinkedBlockingQueue<>();
    static ChatData chatData;

    public SerialIO(Context context, UsbManager usbManager, List<UsbSerialDriver> availableDrivers,
                    UsbSerialDriver driver, UsbSerialPort port) {
        this.context = context;
        this.manager = usbManager;
        this.availableDrivers = availableDrivers;
        this.driver = driver;
        this.port = port;
        SerialIO.portStatic = port;
        chatData = new ChatData(ChatActivity.contextStatic);
    }
    private static class FredOdbieranie implements Runnable{
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                while(connected){
                    int len = 0;
                    boolean error = false, error2= false;
                    byte[] bufor = new byte[1000];
                    try {
                        len=portStatic.read(bufor,2000);
                    } catch (IOException e) {
                        e.printStackTrace();
                        error=true;
                    }
                     if (!error){
                        int i=0;
                        while (i<len){
                            try {
                                received.put(bufor[i]);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                error2 = true;
                            }
                            if (!error2)
                                i++;
                            else
                                error2= false;
                        }
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    static SerialIO.FredOdbieranie fredOdb= new SerialIO.FredOdbieranie();
    private static class FredWriteCD implements Runnable{
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                publicChatExists =chatData.checkChatExists(0);
                while(connected){
                    boolean startedReceivingMessage = false, continueWhile=true, error3;
                    List<Byte> a= new ArrayList<>();
                    if(received.size()!=0) {
                        while(continueWhile&&connected){
                            error3=false;
                            Byte b=null;
                            try {
                                b = received.take();
                               } catch (InterruptedException e) {
                                e.printStackTrace();
                                error3=true;
                            }

                            if (!error3&&b!=null) {
                                if (!startedReceivingMessage){
                                    if (b==(byte)znakPoczatkuWiadomosci){
                                        a.add(b);
                                        startedReceivingMessage= true;
                                    }
                                }
                                else {
                                    if (a.size()<240){
                                          if (b==(byte)znakKoncaWiadomosci){
                                            int cid = Byte.toUnsignedInt(a.get(1));
                                            byte[] m= listaByteDoArrayPrimitive(a.subList(2,
                                                    a.size()));
                                            String ms=new String(m,StandardCharsets.UTF_8);
                                            chatData.writeChatMsg(cid,false,ms);
                                            if (publicChatExists&&cid!=0) {
                                                chatData.writeChatMsg(0, false, ms);
                                            }

                                            startedReceivingMessage=false;
                                            a.clear();
                                            continueWhile=false;
                                        }
                                        else{
                                            if (a.size()==239){
                                                a.clear();
                                                startedReceivingMessage=false;
                                                continueWhile=false;

                                            }
                                            else {
                                                a.add(b);
                                            }

                                        }
                                    }
                                }
                            }


                        }
                    }
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    static SerialIO.FredWriteCD fredWCD= new SerialIO.FredWriteCD();
    static boolean connected =false;
    static boolean publicChatExists;
    void zamknijPort() throws IOException {
        connected=false;
        thread1.interrupt();
        thread4.interrupt();
        port.close();
    }
    static byte[] listaByteDoArrayPrimitive(List<Byte> a){
        byte[] b = new byte[a.size()];
        for (int i=0; i<a.size();i++){
            b[i]=a.get(i);
        }
        return b;
    }
    static List<Integer> buforDoListyInteger(int len, byte[] bufor){
        List<Integer> a= new ArrayList<>();
        for (int i=0; i<len;i++)
            a.add(Byte.toUnsignedInt(bufor[i]));
        return a;
    }

    UsbDeviceConnection connection;
    static Thread thread1 = new Thread(fredOdb);
    static Thread thread4 = new Thread(fredWCD);

    boolean zworkaPodlaczona(){
        byte [] a = new byte[1000];
        byte [] c = new byte[4];
        c[0]=70;c[1]=71;c[2]=72;c[3]=73;
        int len =0;
        List<Integer> b = new ArrayList<>();
        try {
            port.write(c,2000);
            for (int i=0;i<5;i++){
                len = port.read(a,2000);
                b.addAll(SerialIO.buforDoListyInteger(len,a));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!b.isEmpty()){
            for (int i=0; i<b.size()-2;i++)
                if (b.get(i)==255)
                    if (b.get(i+1)==255&&b.get(i+2)==255)
                        return false;
        }
        return true;
    }

    void poczatek() {

        connection = manager.openDevice(driver.getDevice());
        if (connection == null) {
            return;
        }
        try {
            port.open(connection);
        } catch (IOException e) {
            Toast.makeText(context.getApplicationContext(), "error port.open(connection)",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        try {
            port.setParameters(9600, 8, UsbSerialPort.STOPBITS_1,
                    UsbSerialPort.PARITY_NONE);
        } catch (IOException e) {
            Toast.makeText(context.getApplicationContext(), "error port.setParameters",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        if (thread1.getState() == Thread.State.NEW) {
            thread1.setDaemon(true);
            thread1.start();
        }
        if (thread4.getState() == Thread.State.NEW) {
            thread4.setDaemon(true);
            thread4.start();
        }

    }
    void wyslij3(String msg, Integer chatId) throws IOException {
        byte[] b;
        b=msg.getBytes(StandardCharsets.UTF_8);
        int l=b.length;
        byte[] bufferWrite = new byte[l+3];
        assert chatId<256&&chatId>-1;
        System.arraycopy(b, 0, bufferWrite, 2, l);
        bufferWrite[0]=(byte) znakPoczatkuWiadomosci;
        bufferWrite[1]=(byte)chatId.intValue();
        bufferWrite[l+3-1]=(byte) znakKoncaWiadomosci;
        port.write(bufferWrite, 2000);
    }
}
