package com.example.test6;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class SettingsActivity extends AppCompatActivity {
    TextView textView,textView2;
    ImageView imageView;
    Button button1,button2,button3,buttonReset;
    private static final String INTENT_ACTION_GRANT_USB = BuildConfig.APPLICATION_ID + ".GRANT_USB";

    UsbDeviceConnection connection;
    UsbManager manager;
    UsbSerialDriver driver;
    UsbSerialPort port;
    public int fredSwitch=-1;
    ProgressDialog dialogLadowanie;

    private class Fred1 implements Runnable{
        public void run() {
            int kopiaFredSwitch=fredSwitch;
            if (fredSwitch >= 1&&fredSwitch<=4) {
                boolean a = zworkaOdlaczona();
                int[] array = new int[3];
                if (fredSwitch!=4){
                    try {
                        array =odczytajRejestry();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                int[] finalArray = array;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (a) {
                            try {
                                switch (kopiaFredSwitch) {
                                    case 1:
                                        ustawKanalDialog(finalArray);
                                        break;
                                    case 2:
                                        ustawRozmiarPakietuDialog(finalArray);
                                        break;
                                    case 3:
                                        ustawPredkoscDialog(finalArray);
                                        break;
                                    case 4:
                                        if (resetUstawien())
                                            genericDialog("pomyslnie zmieniono ustawienia");
                                        else
                                            genericDialog("nie udalo sie przywrocic ustawien");
                                        break;
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        else
                            genericDialog("nie odlaczono zworki");
                        dialogLadowanie.dismiss();
                    }
                });
            }
            fredSwitch=-1;
            thread1.interrupt();
        }
    }
    private Fred1 fred1= new Fred1();
    Thread thread1 = new Thread(fred1);

    void genericDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Komunikat");
        builder.setMessage(message);
        builder.setCancelable(true);
        AlertDialog dialog;
        DialogInterface.OnClickListener a=null;
        builder.setPositiveButton("zamknij", a);
        dialog= builder.create();
        AlertDialog finalDialog = dialog;
        a = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finalDialog.dismiss();
            }
        };
        dialog.show();
    }
    void genericToast(String message){
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    boolean noConnectedDevices(){
        UsbManager manager = (UsbManager) getApplicationContext().
                getSystemService(Context.USB_SERVICE);
        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().
                findAllDrivers(manager);
        return  availableDrivers.isEmpty();
    }

    void poczatek(){
        manager = (UsbManager) getApplicationContext().getSystemService(Context.USB_SERVICE);
        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().
                findAllDrivers(manager);
        driver =availableDrivers.get(0);
        connection = manager.openDevice(driver.getDevice());
        port = driver.getPorts().get(0);
        if (connection == null) {
            return;
        }
        try {
            port.open(connection);
        } catch (IOException e) {
            genericToast("error port.open(connection)");
            e.printStackTrace();
        }
        try {
            port.setParameters(9600, 8, UsbSerialPort.STOPBITS_1,
                    UsbSerialPort.PARITY_NONE);

        } catch (IOException e) {
            genericToast( "error port.setParameters");
            e.printStackTrace();
        }
    }

    void polacz(){
        UsbManager manager = (UsbManager) getApplicationContext().getSystemService(
                Context.USB_SERVICE);
        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().
                findAllDrivers(manager);
        UsbSerialDriver driver = availableDrivers.get(0);
        UsbSerialPort port = driver.getPorts().get(0);
        int flags = PendingIntent.FLAG_IMMUTABLE;
        PendingIntent usbPermissionIntent = PendingIntent.getBroadcast(getApplicationContext(),
                0, new Intent(INTENT_ACTION_GRANT_USB), flags);
        manager.requestPermission(driver.getDevice(), usbPermissionIntent);
    }
    boolean zworkaOdlaczona(){

        byte [] a = new byte[1000];
        byte [] c = new byte[4];
        c[0]=70;c[1]=71;c[2]=72;c[3]=73;
        int len =0;
        List<Integer> b = new ArrayList<>();
        try {
            port.write(c,2000);
            for (int i=0;i<5;i++){
                len = port.read(a,2000);
                b.addAll(SerialIO.buforDoListyInteger(len,a));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!b.isEmpty())
            for (int i=0; i<b.size()-2;i++)
                if (b.get(i)==255)
                    if (b.get(i+1)==255&&b.get(i+2)==255){
                        return true;
                    }
        return false;
    }
    boolean moznaZmieniacUstawienia(){
        if (!noConnectedDevices()){
            if (SerialIO.connected){
                try {
                    ChatActivity.serial.zamknijPort();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            polacz();
            poczatek();

            return true;
        }
        else{
            genericDialog("Nie podlaczono urzadzenia");
            return false;
        }
    }
    int[] odczytajRejestry() throws IOException {
        byte[] a = new byte[3];
        byte[] b = new byte[1000];
        List<Integer> c = new ArrayList<>();
        a[0]=(byte)0xc1;a[1]=(byte)0x00;a[2]=(byte)0x09;
        port.write(a,2000);
        int len=0;
        for (int i=0;i<15;i++) {
            len = port.read(b, 2000);
            c.addAll(SerialIO.buforDoListyInteger(len,b));
        }
        int d = c.indexOf(193);
        if (d!=-1&&c.size()-d>=10){

            return new int[]{c.get(d+8),(byte)c.get(d+6).intValue()&0x7,
                    c.get(d+7) >>>6};
        }
        else
            return new int[]{255};
    }
    void dialogKoniecOperacji(boolean noError){
        if (noError)
            genericDialog("pomyslnie ustawiono");
        else
            genericDialog("wystapil blad, nie ustawiono");
    }
    boolean noErrorRozmiarPakietu = false;
    void ustawRozmiarPakietuDialog(int[] a) throws IOException {
        if (a[0]==255) {
            genericDialog("nie udalo sie polaczyc z plytka");
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        int b=a[2]+1;
        builder.setTitle("Wybierz rozmiar pakietu w bajtach. Poprzednio ustawiono opcje" +
                " nr "+b);
        String[] wybor = {"240","128","64","32"};
        builder.setItems(wybor, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    noErrorRozmiarPakietu=ustawRozmiarPakietu(which);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                dialogKoniecOperacji(noErrorRozmiarPakietu);
            }
        });
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    boolean ustawRozmiarPakietu(Integer rozmiar) throws IOException {
        byte[] a = new byte[4];
        byte[] b = new byte[1000];
        List<Integer> c = new ArrayList<>();
        if (rozmiar<0 || rozmiar>3)
            return false;
        switch (rozmiar){
            case 0:
                a[3]=(byte)0b00000011;
                break;
            case 1:
                a[3]=(byte)0b01000011;
                break;
            case 2:
                a[3]=(byte)0b10000011;
                break;
            case 3:
                a[3]=(byte)0b11000011;
                break;
        }
        a[0]=(byte)0xc0;a[1]=(byte)0x04;a[2]=(byte)0x01;
        port.write(a,2000);
        int len=0;
        for (int i=0;i<10;i++) {
            len = port.read(b, 2000);
            c.addAll(SerialIO.buforDoListyInteger(len,b));
        }
        int d=c.indexOf(193);
        if (d!=-1&&c.size()-d>=4)
            return c.get(d) == 193 &&c.get(d+1) == 4 && c.get(d+2) == 1&&
                    c.get(d+3) == Byte.toUnsignedInt(a[3]);
        else
            return false;
    }
    boolean noErrorPredkosc = false;
    void ustawPredkoscDialog(int[] a) throws IOException {
        if (a[0]==255) {
            genericDialog("nie udalo sie polaczyc z plytka");
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        int b=a[1]+1;
        builder.setTitle("Wybierz parametry nadawania. Poprzednio ustawiono opcje" +
                " nr "+b);
        String[] wybor = {"0.3kbps, sf=12, bw=125khz","1.2kbps, sf=11, bw=250khz",
                "2.4kbps, sf=11, bw=500khz","4.8kbps, sf=8, bw=250khz","9.6kbps, sf=8, bw=500khz",
                "19.2kbps, sf=7, bw=500khz","38.4kbps, sf=6, bw=500khz","62.5kbps, sf=5, bw=500khz"
        };
        builder.setItems(wybor, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    noErrorPredkosc=ustawPredkosc(which);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                dialogKoniecOperacji(noErrorPredkosc);
            }
        });
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    boolean ustawPredkosc(Integer predkosc) throws IOException {

        byte[] a = new byte[4];
        byte[] b = new byte[1000];
        List<Integer> c = new ArrayList<>();
        if (predkosc<0 || predkosc>8)
            return false;
        switch (predkosc){
            case 0:
                a[3]=(byte)0b01100000;                break;
            case 1:
                a[3]=(byte)0b01100001;                break;
            case 2:
                a[3]=(byte)0b01100010;                break;
            case 3:
                a[3]=(byte)0b01100011;                break;
            case 4:
                a[3]=(byte)0b01100100;                break;
            case 5:
                a[3]=(byte)0b01100101;                break;
            case 6:
                a[3]=(byte)0b01100110;                break;
            case 7:
                a[3]=(byte)0b01100111;                break;
        }
        a[0]=(byte)0xc0;a[1]=(byte)0x03;a[2]=(byte)0x01;
        port.write(a,2000);
        int len=0;
        for (int i=0;i<10;i++) {
            len = port.read(b, 2000);
            c.addAll(SerialIO.buforDoListyInteger(len,b));
        }
        int d=c.indexOf(193);

        if (d!=-1&&c.size()-d>=4)
            return c.get(d) == 193 &&c.get(d+1) == 3 && c.get(d+2) == 1&&
                    c.get(d+3) == Byte.toUnsignedInt(a[3]);
        else
            return false;
    }
    boolean noErrorKanal = false;
    void ustawKanalDialog(int[] a) throws IOException {

        if (a[0]==255) {
            genericDialog("nie udalo sie polaczyc z plytka");
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        int b=a[0]-23+1;
        builder.setTitle("Wybierz kanal nadawania. Poprzednio ustawiono opcje" +
                " nr "+b);
        String[] wybor = {"433125 khz","434125 khz"};
        builder.setItems(wybor, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    noErrorKanal=ustawKanal(which+23);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                dialogKoniecOperacji(noErrorKanal);
            }
        });
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    boolean ustawKanal(Integer kanal) throws IOException {

        byte[] a = new byte[4];
        byte[] b = new byte[1000];
        List<Integer> c = new ArrayList<>();
        if (kanal<23 || kanal>24)
            return false;
        switch (kanal){
            case 23:
                a[3]=(byte)0x17;                break;
            case 24:
                a[3]=(byte)0x18;                break;
        }
        a[0]=(byte)0xc0;a[1]=(byte)0x05;a[2]=(byte)0x01;
        port.write(a,2000);
        int len=0;
        for (int i=0;i<10;i++) {
            len = port.read(b, 2000);
            c.addAll(SerialIO.buforDoListyInteger(len,b));
        }
        int d=c.indexOf(193);

        if (d!=-1&&c.size()-d>=4)
            return c.get(d) == 193 &&c.get(d+1) == 5 && c.get(d+2) == 1&&
                    c.get(d+3) == Byte.toUnsignedInt(a[3]);
        else
            return false;
    }
    boolean resetUstawien(){
        boolean b1 = false,b2 = false,b3 = false;
        try {
            b1=ustawKanal(24);
            b2=ustawRozmiarPakietu(0);
            b3=ustawPredkosc(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return b1 && b2 && b3;
    }
    public void onStop() {
        super.onStop();
        if (connection!=null) {
            try {
                port.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        textView = findViewById(R.id.textview1);
        textView.setText(
                R.string.instrukcja);
        textView2 = findViewById(R.id.textview2);
        textView2.setText(R.string.ostrzezenie);
        imageView = findViewById(R.id.imageview1);
        imageView.setImageResource(R.drawable.plytkalora);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        buttonReset = findViewById(R.id.buttonReset);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ustawienia");
        setSupportActionBar(toolbar);

        if (button1 !=null){
            button1.setOnClickListener((View.OnClickListener)(new View.OnClickListener(){
                public void onClick(View it) {

                    if (moznaZmieniacUstawienia()) {
                        if (connection!=null){
                            thread1= new Thread(fred1);
                            dialogLadowanie = ProgressDialog.show(SettingsActivity.this,
                                    "Komunikat","Ladowanie. Prosze czekac.",
                                    true);
                            if (thread1.getState() == Thread.State.NEW){
                                fredSwitch=1;
                                thread1.start();
                            }
                        }
                        else{
                            genericToast("nie przyznano uprawnienia");
                        }
                    }
                }
            }));
        }

        if (button2 !=null){
            button2.setOnClickListener((View.OnClickListener)(new View.OnClickListener(){
                public void onClick(View it) {
                    if (moznaZmieniacUstawienia()) {
                        if (connection!=null){
                            thread1= new Thread(fred1);
                            dialogLadowanie = ProgressDialog.show(SettingsActivity.this,
                                    "Komunikat","Ladowanie. Prosze czekac.",
                                    true);
                            if (thread1.getState() == Thread.State.NEW){
                                fredSwitch=2;
                                thread1.start();
                            }
                        }
                        else{
                            genericToast("nie przyznano uprawnienia");
                        }
                    }
                }
            }));
        }
        if (button3 !=null){
            button3.setOnClickListener((View.OnClickListener)(new View.OnClickListener(){
                public void onClick(View it) {

                    if (moznaZmieniacUstawienia()) {
                        if (connection!=null){
                            thread1= new Thread(fred1);
                            dialogLadowanie = ProgressDialog.show(SettingsActivity.this,
                                    "Komunikat","Ladowanie. Prosze czekac.",
                                    true);
                            if (thread1.getState() == Thread.State.NEW){
                                fredSwitch=3;
                                thread1.start();
                            }
                        }
                        else{
                            genericToast("nie przyznano uprawnienia");
                        }
                    }
                }
            }));
        }
        if (buttonReset !=null){
            buttonReset.setOnClickListener((View.OnClickListener)(new View.OnClickListener(){
                public void onClick(View it) {

                    if (moznaZmieniacUstawienia()) {
                        if (connection!=null){
                            thread1= new Thread(fred1);
                            dialogLadowanie = ProgressDialog.show(SettingsActivity.this,
                                    "Komunikat","Ladowanie. Prosze czekac. " +
                                            "Zmiana trwa kilkadziesiat sekund",true);
                            if (thread1.getState() == Thread.State.NEW){
                                fredSwitch=4;
                                thread1.start();
                            }
                        }
                        else{
                            genericToast("nie przyznano uprawnienia");
                        }
                    }
                }
            }));
        }

    }
}
