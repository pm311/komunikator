package com.example.test6;
import static android.text.InputType.TYPE_CLASS_NUMBER;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    public ArrayAdapter<String> adapter;
    public ChatData chatData;

    boolean chatIdAnyExists(){
        return chatData.getAllChatNames().size() != 0;
    }

    void dialogNewChat(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Wybierz typ czatu");
        String[] wybor = {"publiczny","prywatny"};
        builder.setItems(wybor, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    if (chatData.checkChatExists(0))
                        genericDialog("Juz stworzono kanal publiczny");
                    else {
                        chatData.createChat(0, "kanal publiczny");
                        adapter.notifyDataSetChanged();
                    }
                }
                if (which == 1)
                    dialogNewChatId();
            }
        });
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void dialogNewChatId(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Prosze wpisac identyfikator czatu");
        EditText chatId = new EditText(this);
        chatId.setInputType(TYPE_CLASS_NUMBER);
        builder.setView(chatId);
        builder.setCancelable(true);
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                if (!String.valueOf(chatId.getText()).equals("")){
                    int nazwa2 = Integer.parseInt(String.valueOf(chatId.getText()));
                    if (nazwa2>0 && nazwa2<254) {
                        if (chatData.checkChatExists(nazwa2)){
                            genericDialog("Czat z tym identyfikatorem juz istnieje");}
                        else{
                            dialogNewChatName(nazwa2);}
                    }
                    else
                        genericDialog("Identyfikator ma byc liczba pomiedzy 1, a 253");
                }
                else{
                    genericDialog("nie podano identyfikatora");
                }}
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void genericDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Komunikat");
        builder.setMessage(message);
        builder.setCancelable(true);
        AlertDialog dialog;
        DialogInterface.OnClickListener a=null;
        builder.setPositiveButton("zamknij", a);
        dialog= builder.create();
        AlertDialog finalDialog = dialog;
        a = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finalDialog.dismiss();
            }
        };
        dialog.show();
    }

    void dialogNewChatName(Integer chatId){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Prosze wpisac nazwe czatu");
        EditText nazwa = new EditText(this);
        builder.setView(nazwa);
        builder.setCancelable(true);
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                String nazwa2 = String.valueOf(nazwa.getText());
                if (nazwa2.equals(""))
                    chatData.createChat(chatId,"czat "+chatId.toString());
                else
                    chatData.createChat(chatId,nazwa2);
                adapter.notifyDataSetChanged();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ChatActivity.thread2.interrupt();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chatData = new ChatData(getApplicationContext());

        listView= findViewById(R.id.listView);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                android.R.id.text1, chatData.allChatNames);
        listView.setAdapter(adapter);


        Button button3 = findViewById(R.id.button3);
        if (button3 !=null){
            button3.setOnClickListener(new View.OnClickListener(){
                public void onClick(View it) {
                    dialogNewChat();
                    adapter.notifyDataSetChanged();
                }
            });
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value=adapter.getItem(position);

                Intent intent = new Intent(MainActivity.this, ChatActivity.
                        class);

                if (chatData.getChatIdByName(value)==null)
                    genericDialog("blad - nie ma takiego czat Id");
                else{
                    intent.putExtra("chatId", chatData.getChatIdByName(value).toString());
                    MainActivity.this.startActivity(intent);
                }
            }
        });

        Button button4 = findViewById(R.id.button4);
        if (button4 !=null){
            button4.setOnClickListener(new View.OnClickListener(){
                public void onClick(View it) {
                    Intent intent2 = new Intent(MainActivity.this,
                            SettingsActivity.class);
                    MainActivity.this.startActivity(intent2);
                }
            });
        }
    }
}
